# [pymake v1.0](https://github.com/nenetto/pymake)

master: [![Build Status](https://gitlab.com/nenetto/pymake/badges/master/pipeline.svg)](https://gitlab.com/nenetto/pymake/commits/master)

develop: [![Develop status](https://gitlab.com/nenetto/pymake/badges/develop/pipeline.svg)](https://gitlab.com/nenetto/pymake/commits/develop)

**Contact**
  - [Eugenio Marinetto](mailto:nenetto@gmail.com)